#ifndef POLICY_H_INCLUDED
#define POLICY_H_INCLUDED

#include "message.h"

/* Ed25519 public key. */
struct public_key {
  uint8_t pub[32];
};

/* Distinct from the merkle inclusion errors */
enum verify_error {
  VERIFY_LEAF_SIGNATURE = 10,
  VERIFY_LOG_SIGNATURE = 11,
  VERIFY_COSIGNATURE = 12,
};

typedef int verify_func(const struct public_key *pub, const struct signature *sig,
			size_t size, const uint8_t *data);

/* Policy requiring a specific log, a specific cosigning witness, and
   a specific submitter key. */
struct policy {
  struct public_key submitter_key;
  struct public_key log_key;
  struct public_key witness_key;
  /* Indirection, to call separate implementations (omonocypher on
     device, nettle on the host) */
  verify_func *verify;
};

void
policy_get_witness_key_hash(const struct policy *p, struct merkle_hash *kh);

/* Returns 0 in success, otherwise a verify_error or merkle_error constant. */
int
policy_verify(const struct policy *p, const struct message *msg);

#endif /* POLICY_H_INCLUDED */
