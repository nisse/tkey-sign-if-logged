#include "encode.h"

size_t
encode_hex(uint8_t *p, size_t size, const uint8_t *data)
{
  static const char digits[16] = "0123456789abcdef";
  size_t i;
  for (i = 0; i < size; i++)
    {
      *p++ = digits[data[i] >> 4];
      *p++ = digits[data[i] & 0xf];
    }
  return 2*size;
}

struct qr {
  uint64_t q;
  uint64_t r;
};

static void
divrem10(struct qr *qr, uint64_t x)
{
  uint64_t p;
  assert ((x >> 32) == 0);
  /* The magic constant is 1 + floor(2^32 / 10) */
  qr->q = (x * 0x1999999a) >> 32;
  p = 10 * qr->q;
  /* For lack of proper error analysis. */
  while (p > x) 
    {
      p -= 10;
      qr->q--;
    }
  qr->r = x - p;
  assert (qr->r < 10);
}

size_t
encode_decimal(uint8_t *p, uint64_t x)
{
  uint8_t buf[20]; /* Maximum size for uint64_t, 18446744073709551615 */
  unsigned n, i;

  if (x == 0)
    {
      *p = '0';
      return 1;
    }
  /* Generate digits in reverse order */
  for (n = 0; x > 0; n++)
    {
      struct qr qr;
      assert (n < 20);
      divrem10 (&qr, x);
      buf[n] = qr.r; /* x % 10u */
      x = qr.q; /* x = x / 10u */
    }
  for (i = 0; i < n; i++)
    p[i] = '0' + buf[n-i-1];

  return n;
}

size_t
encode_base64(uint8_t *p, size_t size, const uint8_t *data)
{
  static const char digits[64] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";
  size_t pos;

  /* 3 octets is 24 bits, transformed to 4 base 64 characters. */
  for (pos = 0; size >= 3; size -= 3, data += 3)
    {
      p[pos++] = digits[data[0] >> 2];
      p[pos++] = digits[((data[0] & 0x3) << 4) | data[1] >> 4];
      p[pos++] = digits[((data[1] & 0xf) << 2) | data[2] >> 6];
      p[pos++] = digits[data[2] & 0x3f];
    }
  // The only case we use */
  assert (size == 2);
  p[pos++] = digits[data[0] >> 2];
  p[pos++] = digits[((data[0] & 0x3) << 4) | data[1] >> 4];
  p[pos++] = digits[((data[1] & 0xf) << 2)];
  p[pos++] = '=';

  return pos;
}
