#ifndef ENCODE_H_INCLUDED
#define ENCODE_H_INCLUDED

#include "platform.h"

/* Lowercase hex encoding, for checkpoint and related formats. */
size_t
encode_hex(uint8_t *p, size_t size, const uint8_t *data);

size_t
encode_decimal(uint8_t *p, uint64_t x);

size_t
encode_base64(uint8_t *p, size_t size, const uint8_t *data);

#endif /* ENCODE_H_INCLUDED */
