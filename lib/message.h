#ifndef MESSAGE_H_INCLUDED
#define MESSAGE_H_INCLUDED

#include "merkle.h"

#define MAX_INCLUSION_PATH 32

/* Ed25519 signature */
struct signature {
  uint8_t s[64];
};

/* A parsed message. */
struct message {
  size_t data_size;
  /* The data points directly into the parsed message, and depends on that data staying valid. */
  const uint8_t *data;
  struct signature leaf_signature;
  struct merkle_tree_head tree_head;
  struct signature log_signature;
  struct signature cosignature;
  uint64_t timestamp;
  uint64_t leaf_index;
  unsigned path_length;
  struct merkle_hash path[MAX_INCLUSION_PATH];
};

/* Parses input data, and initializes the message struct with values
   and pointers into the data. The keyhash identifies the cosignature
   we're interested in. Returns 1 on success, 0 on failure. */
int
message_parse (struct message *msg, size_t n, const uint8_t *data, const struct merkle_hash *keyhash);

#endif /* MESSAGE_H_INCLUDED */
