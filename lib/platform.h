/* Deal with standard includes not in the usual place in the tkey build */
#ifndef PLATFORM_H_INCLUDED

#ifdef TKEY_BUILD
/* From tkey libs */
#include "tkey/lib.h"
#define assert(x)
#else
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#endif

#define PLATFORM_H_INCLUDED
#endif /* PLATFORM_H_INCLUDED */
