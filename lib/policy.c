#include "encode.h"
#include "policy.h"

static void
hash_data(struct merkle_hash *hash, size_t size, const uint8_t *data)
{
  struct sha256_ctx ctx;
  sha256_init (&ctx);
  sha256_update (&ctx, size, data);
  sha256_digest (&ctx, sizeof(hash->h), hash->h);
}

void
policy_get_witness_key_hash(const struct policy *p, struct merkle_hash *kh)
{
  hash_data (kh, sizeof(p->witness_key.pub), p->witness_key.pub);
}

int
policy_verify_leaf_signature(const struct policy *p, const struct signature *sig, const struct merkle_hash *checksum)
{
  static const uint8_t prefix[] = "sigsum.org/v1/tree-leaf"; /* Include the NUL byte. */
  uint8_t data[sizeof(prefix) + 32];
  memcpy (data, prefix, sizeof(prefix));
  memcpy (data + sizeof(prefix), checksum->h, 32);
  return p->verify(&p->submitter_key, sig, sizeof(data), data);
}

int
policy_verify_tree_signatures(const struct policy *p, const struct message *msg)
{
  static const uint8_t cosignature_prefix[20] = "cosignature/v1\ntime "; /* NUL byte not included. */
  static const uint8_t checkpoint_prefix[19] = "sigsum.org/v1/tree/"; /* NUL byte not included. */
  struct merkle_hash kh;
  uint8_t data[1000];
  size_t pos = 0;
  size_t checkpoint_start;

  memcpy (data, cosignature_prefix, sizeof(cosignature_prefix)); pos += sizeof(cosignature_prefix);
  pos += encode_decimal (data + pos, msg->timestamp);
  data[pos++] = '\n';

  /* Start of the checkpoint. */
  checkpoint_start = pos;
  hash_data (&kh, sizeof(p->log_key.pub), p->log_key.pub);

  memcpy (data + pos, checkpoint_prefix, sizeof(checkpoint_prefix)); pos += sizeof(checkpoint_prefix);
  pos += encode_hex (data + pos, sizeof(kh.h), kh.h);
  data[pos++] = '\n';
  pos += encode_decimal (data + pos, msg->tree_head.size);
  data[pos++] = '\n';
  pos += encode_base64 (data + pos, sizeof(msg->tree_head.root_hash.h), msg->tree_head.root_hash.h);
  data[pos++] = '\n';
  assert (pos < sizeof(data));

  if (!p->verify(&p->log_key, &msg->log_signature, pos - checkpoint_start, data + checkpoint_start))
    return VERIFY_LOG_SIGNATURE;

  if (!p->verify(&p->witness_key, &msg->cosignature, pos, data))
    return VERIFY_COSIGNATURE;

  return 0;
}

static void
hash_leaf_node (struct merkle_hash *leaf_hash, const struct merkle_hash *checksum,
		const struct signature *sig, const struct public_key *key)
{
  struct merkle_hash kh;
  struct sha256_ctx ctx;
  hash_data (&kh, sizeof(key->pub), key->pub);
  sha256_init (&ctx);
  sha256_update (&ctx, sizeof(merkle_leaf_prefix), merkle_leaf_prefix);
  sha256_update (&ctx, sizeof(checksum->h), checksum->h);
  sha256_update (&ctx, sizeof(sig->s), sig->s);
  sha256_update (&ctx, sizeof(kh.h), kh.h);
  sha256_digest (&ctx, sizeof(leaf_hash->h), leaf_hash->h);
}

int
policy_verify(const struct policy *p, const struct message *msg)
{
  struct merkle_hash checksum;
  struct merkle_hash leaf_hash;
  int err;
  hash_data (&checksum, msg->data_size, msg->data);
  hash_data (&checksum, sizeof(checksum.h), checksum.h);

  if (!policy_verify_leaf_signature (p, &msg->leaf_signature, &checksum))
    return VERIFY_LEAF_SIGNATURE;

  if ((err = policy_verify_tree_signatures (p, msg)) != 0)
    return err;

  hash_leaf_node (&leaf_hash, &checksum, &msg->leaf_signature, &p->submitter_key);
  return merkle_verify_inclusion (&leaf_hash, msg->leaf_index,
				  &msg->tree_head, msg->path_length, msg->path);
}
