/* Merkle tree and inclusion proof, see RFC 9162 */

#ifndef MERKLE_H_INCLUDED
#define MERKLE_H_INCLUDED

#include "sha256.h"

typedef uint64_t merkle_size_t;

extern const uint8_t merkle_leaf_prefix[1];

struct merkle_hash
{
  uint8_t h[SHA256_DIGEST_SIZE];
};

struct merkle_tree_head
{
  uint64_t size;
  struct merkle_hash root_hash;
};

enum merkle_error {
  MERKLE_OK = 0,
  MERKLE_OUT_OF_RANGE,
  MERKLE_TOO_LONG,
  MERKLE_TOO_SHORT,
  MERKLE_HASH_MISMATCH,
};

int
merkle_hash_equal(const struct merkle_hash *a, const struct merkle_hash *b);

void
merkle_hash_interior_node(struct merkle_hash *r, const struct merkle_hash *a, const struct merkle_hash *b);


int
merkle_verify_inclusion(const struct merkle_hash *leaf_hash, merkle_size_t leaf_index,
			const struct merkle_tree_head *tree_head, unsigned path_length,
			const struct merkle_hash *path);

#endif /* MERKLE_H_INCLUDED */
