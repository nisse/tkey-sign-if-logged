#include "merkle.h"

#ifdef TKEY_BUILD
static int
memcmp(const uint8_t *a, const uint8_t *b, size_t n)
{
  size_t i;
  for (i = 0; i < n; i++)
    {
      if (a[i] < b[i]) return -1;
      if (a[i] > b[i]) return 1;
    }
  return 0;
}
#else
#include <string.h>
#endif

const uint8_t merkle_leaf_prefix[1] = { 0 };

int
merkle_hash_equal(const struct merkle_hash *a, const struct merkle_hash *b)
{
  return memcmp(a->h, b->h, SHA256_DIGEST_SIZE) == 0;
}

void
merkle_hash_interior_node(struct merkle_hash *r, const struct merkle_hash *a, const struct merkle_hash *b)
{
  struct sha256_ctx ctx;
  static const uint8_t prefix = 1;
  sha256_init (&ctx);
  sha256_update (&ctx, 1, &prefix);
  sha256_update (&ctx, sizeof(a->h), a->h);
  sha256_update (&ctx, sizeof(b->h), b->h);
  sha256_digest (&ctx, sizeof(r->h), r->h);
}

#define ODD(x) (((x)&1))
#define EVEN(x) (!ODD(x))

int
merkle_verify_inclusion(const struct merkle_hash *leaf_hash, merkle_size_t leaf_index,
			const struct merkle_tree_head *tree_head, unsigned path_length,
			const struct merkle_hash *path)
{
  struct merkle_hash r;
  unsigned i;

  if (leaf_index >= tree_head->size) /* Always true if tree_size == 0 */
    return MERKLE_OUT_OF_RANGE;

  r = *leaf_hash;

  if (tree_head->size > 1)
    {
      merkle_size_t fn, sn;
      fn = leaf_index;
      sn = tree_head->size - 1;

      for (i = 0; i < path_length; i++)
	{
	  if (sn == 0)
	    return MERKLE_TOO_LONG;

	  if (ODD(fn) || fn == sn)
	    {
	      merkle_hash_interior_node (&r, &path[i], &r);

	      if (EVEN(fn)) /* Only possible in the fn == sn case? */
		do
		  {
		    fn >>= 1;
		    sn >>= 1;
		  }
		while (fn > 0 && EVEN(fn));
	    }
	  else
	    merkle_hash_interior_node (&r, &r, &path[i]);

	  fn >>= 1;
	  sn >>= 1;
	}
      if (sn > 0)
	return MERKLE_TOO_SHORT;
    }
  return merkle_hash_equal (&r, &tree_head->root_hash) ? MERKLE_OK : MERKLE_HASH_MISMATCH;
}
