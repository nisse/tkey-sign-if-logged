#include "message.h"

/* Reads a 16-bit integer, in network, big-endian, byte order */
#define READ_UINT16(p)				\
( (((unsigned) (p)[0]) << 8) | ((unsigned) (p)[1]))

/* Reads a 64-bit integer, in network, big-endian, byte order */
#define READ_UINT64(p)				\
(  (((uint64_t) (p)[0]) << 56)			\
 | (((uint64_t) (p)[1]) << 48)			\
 | (((uint64_t) (p)[2]) << 40)			\
 | (((uint64_t) (p)[3]) << 32)			\
 | (((uint64_t) (p)[4]) << 24)			\
 | (((uint64_t) (p)[5]) << 16)			\
 | (((uint64_t) (p)[6]) << 8)			\
 |  ((uint64_t) (p)[7]))

#define GET_UINT16(res, n, data) do {		\
    if (n < 2) return 0;			\
    res = READ_UINT16(data); n -= 2; data += 2; \
  } while (0)

#define GET_UINT64(res, n, data) do {		\
    if (n < 8) return 0;			\
    res = READ_UINT64(data); n -= 8; data += 8; \
  } while (0)

#define GET_DATA(res, size, n, data) do { \
    if (n < size) return 0;		  \
    res = data; n -= size; data += size;  \
  } while (0);

#define GET_CAST_DATA(res, n, data) do {	\
    const uint8_t *get_tmp;			\
    GET_DATA(get_tmp, sizeof(*res), n, data);	\
    res = (typeof(res)) get_tmp;		\
  } while (0);

/* The res argument must be a pointer type. */
#define COPY_DATA(res, n, data) do { \
    if (n < sizeof(*res)) return 0;		\
    memcpy (res, data, sizeof(*res));		\
    n -= sizeof(*res); data += sizeof(*res);	\
  } while (0)

int
message_parse (struct message *msg, size_t n, const uint8_t *data,
	       const struct merkle_hash *keyhash)
{
  unsigned cosignature_size, proof_size, i;
  int found;

  /* Parse message, 16-bit length prefix. */
  GET_UINT16 (msg->data_size, n, data);
  GET_DATA (msg->data, msg->data_size, n, data);

  /* Parse leaf signature. */
  COPY_DATA (&msg->leaf_signature, n, data);

  /* Parse tree head. */
  GET_UINT64 (msg->tree_head.size, n, data);
  COPY_DATA (&msg->tree_head.root_hash, n, data);

  /* Parse signatures */
  COPY_DATA (&msg->log_signature, n, data);

  GET_UINT16(cosignature_size, n, data);
  if (n < cosignature_size)
    return 0;

  for (found = 0; cosignature_size >= 104; cosignature_size -= 104)
    {
      const struct merkle_hash *kh;
      uint64_t timestamp;
      const struct signature *s;
      GET_CAST_DATA (kh, n, data);
      GET_UINT64 (timestamp, n, data);
      GET_CAST_DATA (s, n, data);
      if (merkle_hash_equal (kh, keyhash))
	{
	  msg->cosignature = *s;
	  msg->timestamp = timestamp;
	  found = 1;
	}
      else
	{
	  n -= 64; data += 64;
	}
    }
  if (cosignature_size != 0 || !found)
    return 0;

  /* Parse proof. */
  GET_UINT64 (msg->leaf_index, n, data);
  GET_UINT16 (proof_size, n, data);
  for (i = 0, msg->path_length = 0; i < MAX_INCLUSION_PATH && proof_size >= 32; i++, proof_size -= 32)
    {
      COPY_DATA (&msg->path[i], n, data);
      msg->path_length++;
    }

  return proof_size == 0 && n == 0;
}
