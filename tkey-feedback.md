# Makefile issues

On debian, almost worked after just setting CC=clang-15 and
OBJCOPY=llvm-objcopy-15 on the make command line. One issue with
llvm-ar, needs to be similarly replaced. And there seems to be a bug
in debian clang-15 packaging which makes it invoke the wrong version
of ld.lld (worker around by manually making a new symlink to the right
version).

# Device C Headers

There is a <stdint.h> (provided by clang, I guess), but incompatible
with tkey types.h. In general, it would be nice with dummy or usable
versions of more standard headers, e.g., assert.h and stdio.h (stdio.h
could provide FILE*, stdin, stdout, stderr, getc, putc and similar,
without the printf machinery).

For naming, it would make sense if the advertised way for apps to use
tkey specific headers were #include <tkey/foo.h> or similar; just
"lib.h" and "types.h" is a bit too generic.

# Division 

Compiling code like

      buf[n] = x % 10u;
      x = x / 10u;

results in calls to __umoddi3 and __udivdi3 which are undefined, and
hence a link time error. Adding -O2 appears to eliminate the call to
__umoddi3, but not to the div function.

I would suggest an implementation based on
https://gmplib.org/~tege/division-paper.pdf, first computing a 32-bit
reciprocal (alg 3 in the paper); then there are a couple of options on
how to use that, depending on operand sizes. I'm a bit surprised clang
doesn't do that already, at least for the case of a compile-time
constant divisor.

# Debugging

Haven't yet tried qemu debugging. But for debugging things on the
actual hardware, it would be nice to have an easy way to send debug
messages to the host, maybe wrapped as some special debug message in
the framing protool (I haven't looked into the details of the
framing). Or two message types, text that the host program can send as
is to its stderr, and raw binary that the host program shouldn't send
to the terminal, but hex encode or save to a file or somthing like
that.

Ideally, a stderr that is hooked up either via serial/usb to the host,
or to qemu, depending on the way the app is run.
