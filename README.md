# Tkey app to sign only if data already sigsum logged

## Beware

This was hacked together during a Tillitis hackathon May 2023.
Hence, it's quality is appropriate for a hackathon prototype.

## Project overview

The idea is to extend the existing TKey Ed25519 signer so that a signature
operation only takes place if the following _sigsum trust policy_ is satisfied:

  - The message to be TKey-signed must first be sigsum-signed by a secondary
    key-pair for which the private key _does not reside on the TKey_
  - The sigsum-signed message must also be logged in a sigsum log
  - The log's signed tree head must be cosigned by at least one witness

The secondary public key, the log's public key, and at least one witness public
key is hard-coded into the TKey device application that is loaded and measured.
So, any change to the Sigsum trust policy will result in a different TKey CDI.

The benefit of this setup is that the user becomes aware of every possible TKey
signature operation by monitoring a Sigsum log (i.e., looking for entries where
the key hash matches the secondary key-pair that does not reside on the TKey).
Notably this does not require anyone else than the TKey user to use Sigsum, thus
offering the benefits of Sigsum without changing verifiers in legacy systems.

The intuition for our extension to the TKey signer is as follows:

  - In the same way that the message to-be-signed is transmitted to the TKey
    right now, submit more bytes to also encode a Sigsum signature and proof.
    The format of the updated message is specified with TLS-encoding below.
  - Unpack message and verify it with the device application's sigsum policy
  - Output TKey signature (as before) iff the Sigsum proof is valid

[TLS-encoded][] message sent from host application to device application:

    opaque data_t<0..2^16-1>;
    opaque signature_t[64];
    opaque hash_t[32];

    struct {
        signature_t signature;
        // other fields can be derived from data and trust policy
    } leaf_t;

	struct {
		hash_t keyhash;
		uint64_t timestamp;
		signature_t signature;
    } cosignature_t;

    struct {
        uint64 tree_size;
        hash_t root_hash;
        signature_t signature;
        cosignature_t cosignatures<1..2^16-1>;
    } cosigned_tree_head_t;
    
    struct {
        uint64 leaf_index;
        hash_t node_hash<0..2^16-1>;
    } proof_t;
    
    struct {
        data_t tbs;
        leaf_t leaf;
        cosigned_tree_head_t cth;
        proof_t proof;
    } message_t;

[TLS-encoded]: https://datatracker.ietf.org/doc/html/rfc5246#section-4

## Current state (as of 2023-05-25)

There's something wrong with how the device app verifies signatures
(by calling monocypher's `crypto_ed25519_check`). When running on an
actual device, verification returns failure.

When running in qemu, that call appears to hang forever. From the debug
printouts, input to the verify operation was
```
 key 3cc85d8a04694370cbf5a306e56a9835
c9430c008ca8e042c82ae40d190af586

 sig e8b924141f70a4b4fa060fd0687e1732
d8819226727f2435d6217cdea668670d
b9c8d9571c4783657987668ecfd220a5
31cc485850c364e99d9fc264c40f9c0f

 msg 73696773756d2e6f72672f76312f7472
65652d6c65616600c7ade88fc7a21498
a6a5e5c385e1f68bed822b72aa63c4a9
a48a02c2466ee29e
```
which is a valid signature. Thus, some further debugging is needed.

## Tools

Cross-compile tools listed on https://dev.tillitis.se/tools/, most
important seems to be `apt-get install clang-15`.

## Libs

`git clone https://github.com/tillitis/tkey-libs.git`

Had to patch Makefile to not run `llvm-ar` (since it's llvm-ar-15 on
my system). Plain ar may work too.

And `ln -sf /bin/ld.lld-15 /bin/ld.lld` (`apt install lld-15`), as well as set
`OBJCOPY` appropriately (similar to `CC` with `-15`).

## Running the stuff

First clone and build [https://github.com/tillitis/tillitis-key1-apps.git], for
the tkey-runapp tool.

Build everything, make and make check in the top-level directory.

To make go tools happy, it's easiest if we first cd into the host
directory.

Insert the tkey and run `../../tillitis-key1-apps/tkey-runapp ../app/app.bin` to load our device app.

Run the host signer tool, e.g.:

    go run ./cmd/tkey-sign/main.go -p`.
    echo -n "foo" > config/message && go run ./cmd/tkey-sign/main.go --policy config/sigsum.policy --submit-key config/submitter-key --fake-witness-key config/fake-witness-key config/message
