#include <string.h>

#include "sha256.h"

#include "testutils.h"

static void
test_one (const char *msg, const char *hex_digest)
{
  struct sha256_ctx ctx;
  uint8_t digest[SHA256_DIGEST_SIZE];
  uint8_t ref[SHA256_DIGEST_SIZE];
  decode_hex(sizeof(ref), ref, hex_digest);
  sha256_init (&ctx);
  sha256_update (&ctx, strlen(msg), (const uint8_t*) msg);
  sha256_digest (&ctx, sizeof(digest), digest);
  if (memcmp (digest, ref, SHA256_DIGEST_SIZE) != 0)
    die("Failed for input \"%s\"\n", msg);
}

int
main (int argc, char **argv)
{
  /* From FIPS180-2 */
  test_one ("abc",
	    "ba7816bf8f01cfea414140de5dae2223"
	    "b00361a396177a9cb410ff61f20015ad");

  test_one ("abcdbcdecdefdefgefghfghighij"
	    "hijkijkljklmklmnlmnomnopnopq",
	    "248d6a61d20638b8e5c026930c3e6039"
	    "a33ce45964ff2167f6ecedd419db06c1");

  test_one ("abcdefghbcdefghicdefghijdefg"
	    "hijkefghijklfghijklmghijklmn"
	    "hijklmnoijklmnopjklmnopqklmn"
	    "opqrlmnopqrsmnopqrstnopqrstu",
	    "cf5b16a778af8380036ce59e7b049237"
	    "0b249b11e8f07a51afac45037afee9d1");
}
