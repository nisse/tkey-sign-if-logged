#include <stdio.h>
#include <string.h>

#include "message.h"
#include "testutils.h"


int
main(int argc, char **argv)
{
  // the test message used in ../host/formats/formats_test.go
  const char *test_msg_hex =
    "00046162636405050000000000000000000000000000000000000000000000000000000000"
    "00000000000000000000000000000000000000000000000000000000000000000000000000"
    "00000002040400000000000000000000000000000000000000000000000000000000000005"
    "05000000000000000000000000000000000000000000000000000000000000000000000000"
    "000000000000000000000000000000000000000000000000000000d0040400000000000000"
    "00000000000000000000000000000000000000000000000000000000000003050500000000"
    "00000000000000000000000000000000000000000000000000000000000000000000000000"
    "00000000000000000000000000000000000000000004040000000000000000000000000000"
    "00000000000000000000000000000000000000000000000305050000000000000000000000"
    "00000000000000000000000000000000000000000000000000000000000000000000000000"
    "00000000000000000000000000000000000000000001002004040000000000000000000000"
    "00000000000000000000000000000000000000";

  uint8_t test_msg[strlen(test_msg_hex) / 2];
  decode_hex(sizeof(test_msg), test_msg, test_msg_hex);

  struct message msg;
  static const struct merkle_hash test_hash = {{4,4}}; // 04040000..00
  static const struct signature test_sig = {{5,5}}; // 05050000..00

  if (!message_parse(&msg, sizeof(test_msg), test_msg, &test_hash))
    die("failed to parse message\n");
  
  // data_t
  if (msg.data_size != 4)
    die("wrong data_size: %ld\n", msg.data_size);
  if (memcmp(msg.data, "abcd", 4))
    die("wrong data\n");

  // leaf_t
  if (memcmp(msg.leaf_signature.s, test_sig.s, 64))
    die("wrong leaf signature\n");

  // cosigned_tree_head_t
  if (msg.tree_head.size != 2)
    die("wrong tree size: %ld\n", msg.tree_head.size);
  if (memcmp(msg.tree_head.root_hash.h, test_hash.h, 32))
    die("wrong root hash\n");
  if (memcmp(msg.log_signature.s, test_sig.s, 64))
    die("wrong log signature\n");
  if (memcmp(msg.cosignature.s, test_sig.s, 64))
    die("wrong cosignature\n");
  if (msg.timestamp != 3)
    die("wrong cosignature timestamp\n");
  if (msg.leaf_index != 1)
    die("wrong leaf index\n");
  if (msg.path_length != 1)
    die("wrong path length\n");
  if (memcmp(msg.path[0].h, test_hash.h, 32))
    die("wrong node hash\n");

  // data is "xyz"
  const char *test_msg2_hex = "000378797a837a5885e5c5816c654de337e9780929434bad03747ce4a2ce233507430c89f99a8bc03ad8bfe75331e285d57247dbd02b5bfaa613642611fe9f7df81f8aeb0a000000000000050023601a71ef1195e8fa670f33958d3b18cb19812f4ada65f32528afa6acbc88439e223ac58e6121c6c516125f586b3c088e87748412f71b211b2e8788b34a41d3b68b877b0be89009e6df023a19817126885d15aa9ea113d1650a4a2d9b3f04040068366f65eee03b54a5f52f472e3e7c0b7820f0b2fe72481e540fe825ca1486f08c00000000646de1ffc9cf52a492bb52ef09eea95f7b848c5c2081bb53c90bb216382b235bcfbf41005fb4cb3c7adec0e52a3ba10142741b1d072add08285420be1eb01d530652690900000000000004ff0120b01cb54602ff3b3835c4c0a5e690f9dbbb819d6b1062651053d488d171defb0a39caa56614499ce392f53f7bd0e50612650bbddb34e4d14c4b35826d53898db59599341b7734031bf6bd3a9edf3782e1f7951f216b228c2eb4003fb9351bbacc8f3dbb7b6aebe7745d08fcb17214d1bc98dbd4a628e0770b4987551cef8715fa9701e2aa25c978564bf5a59aa1a40064a17f6e741c5a108e5b2e5623d3a96fb2b1e4193fd5e6f1a767716a29873125f268d8d16b919de520b05986e19d4f3b6a39925424d8734aee941b60ef2e00421213288f8d30fd4a12f6060b7e1fe824e2335c10aa94ec8dca25233526069f1ed527ef80d832add6c71c76e6b235027cdaf083a6caeb32773e7b8928d77b26461fabdd70a89b9a6973c50864c50f531fe7";

  uint8_t test_msg2[strlen(test_msg2_hex) / 2];
  decode_hex(sizeof(test_msg2), test_msg2, test_msg2_hex);

  const char *witness_kh_hex = "366f65eee03b54a5f52f472e3e7c0b7820f0b2fe72481e540fe825ca1486f08c";
  struct merkle_hash witness_kh;
  decode_hex(sizeof(witness_kh.h), witness_kh.h, witness_kh_hex);

  if (!message_parse(&msg, sizeof(test_msg2), test_msg2, &witness_kh))
    die("failed to parse message\n");

  return 0;
}
