#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "testutils.h"

#include "merkle.h"


static void
hash_leaf_node(struct merkle_hash *r, size_t size, const uint8_t *data)
{
  struct sha256_ctx ctx;
  sha256_init (&ctx);
  sha256_update (&ctx, sizeof(merkle_leaf_prefix), merkle_leaf_prefix);
  sha256_update (&ctx, size, data);
  sha256_digest (&ctx, sizeof(r->h), r->h);
}

static void
test_leaf_hash(const char *data, const char *want_hex)
{
  struct merkle_hash got, want;
  decode_hex (sizeof(want.h), want.h, want_hex);

  hash_leaf_node (&got, strlen(data), (const uint8_t *) data);
  if (!merkle_hash_equal (&got, &want))
    die ("Invalid leaf hash for input: %s\n", data);
}

static void
test_interior_hash(const char *left_hex, const char *right_hex, const char *want_hex)
{
  struct merkle_hash left, right, got, want;
  decode_hex (sizeof(left.h), left.h, left_hex);
  decode_hex (sizeof(right.h), right.h, right_hex);
  decode_hex (sizeof(want.h), want.h, want_hex);

  merkle_hash_interior_node (&got, &left, &right);
  if (!merkle_hash_equal (&got, &want))
    die ("Invalid interior hash\n");
}

static void
test_inclusion(int want, const char *data, merkle_size_t index, merkle_size_t size,
	       const char *root_hash, const char **audit_path, unsigned n)
{
  struct merkle_tree_head tree_head;
  struct merkle_hash path[n];
  struct merkle_hash leaf_hash;
  unsigned i;
  int res;

  tree_head.size = size;
  decode_hex (sizeof (tree_head.root_hash.h), tree_head.root_hash.h, root_hash);

  for (i = 0; i < n; i++)
    decode_hex (sizeof(path[i].h), path[i].h, audit_path[i]);

  hash_leaf_node (&leaf_hash, strlen(data), (const uint8_t *) data);
  res = merkle_verify_inclusion (&leaf_hash, index, &tree_head, n, path);
  if (res != want)
    die ("Unwanted merkle_verify_inclusion value, got %d, want %d\n",
	 res, want);
}

int
main (int argc, char **argv) {
  test_leaf_hash("0",
      "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03");

  test_interior_hash(
      "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03",
      "2215e8ac4e2b871c2a48189e79738c956c081e23ac2f2415bf77da199dfd920c",
      "cb00989d94a569c0a678ae042b63dcd4625db96440517f37a6eb7976ea24ed4b");

  test_inclusion(MERKLE_OK, "0", 0, 1,
      "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03",
      NULL, 0);

  test_inclusion(MERKLE_OK, "1", 1, 4,
      "9f4a3fc20d4162dc37d4e23d907848731a76043ffff6d69288bf1abfbcff478e",
      (const char *[]){
        "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03",
        "d51f2dfecb59566dabdbb6b40bf651cdf39e677b4425165e217590ff3e010edb"
      }, 2);
  test_inclusion(MERKLE_OUT_OF_RANGE, "4", 4, 4,
      "9f4a3fc20d4162dc37d4e23d907848731a76043ffff6d69288bf1abfbcff478e",
      (const char *[]){
        "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03",
        "d51f2dfecb59566dabdbb6b40bf651cdf39e677b4425165e217590ff3e010edb"
      }, 2);
  test_inclusion(MERKLE_HASH_MISMATCH, "asdf", 1, 4,
      "9f4a3fc20d4162dc37d4e23d907848731a76043ffff6d69288bf1abfbcff478e",
      (const char *[]){
        "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03",
        "d51f2dfecb59566dabdbb6b40bf651cdf39e677b4425165e217590ff3e010edb"
      }, 2);
  test_inclusion(MERKLE_TOO_SHORT, "1", 1, 4,
      "9f4a3fc20d4162dc37d4e23d907848731a76043ffff6d69288bf1abfbcff478e",
      (const char *[]){
        "d51f2dfecb59566dabdbb6b40bf651cdf39e677b4425165e217590ff3e010edb",
      }, 1);
  test_inclusion(MERKLE_TOO_LONG, "1", 1, 4,
      "9f4a3fc20d4162dc37d4e23d907848731a76043ffff6d69288bf1abfbcff478e",
      (const char *[]){
        "db3426e878068d28d269b6c87172322ce5372b65756d0789001d34835f601c03",
        "d51f2dfecb59566dabdbb6b40bf651cdf39e677b4425165e217590ff3e010edb",
        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
      }, 3);

  test_inclusion(MERKLE_OK, "2", 2, 7,
      "a3e23b32ccb6bf96d092d165d8aa546e09829de8f03b0e8957581d1e16b92bdf",
      (const char *[]){
        "906c5d2485cae722073a430f4d04fe1767507592cef226629aeadb85a2ec909d",
        "cb00989d94a569c0a678ae042b63dcd4625db96440517f37a6eb7976ea24ed4b",
        "973f083957c7359fb1943acf9e6689bca6ca5ea7197d808aad3c14498689efe0"
      }, 3);

  return 0;
}
