#include <string.h>

#include "encode.h"

#include "testutils.h"

static void
test_decimal(uint64_t x, const char *ref)
{
  uint8_t buf[100];
  size_t res;
  memset (buf, 17, sizeof(buf));

  res = encode_decimal (buf, x);
  if (res != strlen (ref))
    die ("encode_decimal: Unexpected result length %zd for input %lu\n", res, x);

  if (memcmp (buf, ref, strlen(ref)) != 0)
    die ("encode_decimal: Unexpected result for input %lu\n", x);

  if (buf[res] != 17)
    die ("encode_decimal: Buffer overwrite for input %lu\n", x);
}

static void
test_hex (const char *s, const char *ref)
{
  uint8_t buf[100];
  size_t res;
  memset (buf, 17, sizeof(buf));

  res = encode_hex (buf, strlen(s), (const uint8_t *) s);
  if (res != strlen (ref))
    die ("encode_hex: Unexpected result length %zd for input %s\n", res, s);

  if (memcmp (buf, ref, strlen(ref)) != 0)
    die ("encode_hex: Unexpected result for input %s\n", s);

  if (buf[res] != 17)
    die ("encode_hex: Buffer overwrite for input %s\n", s);
}

static void
test_base64 (const char *s, const char *ref)
{
  uint8_t buf[100];
  size_t res;
  memset (buf, 17, sizeof(buf));

  res = encode_base64 (buf, strlen(s), (const uint8_t *) s);
  if (res != strlen (ref))
    die ("encode_base64: Unexpected result length %zd for input %s\n", res, s);

  if (memcmp (buf, ref, strlen(ref)) != 0)
    die ("encode_base64: Unexpected result for input %s\n", s);

  if (buf[res] != 17)
    die ("encode_base64: Buffer overwrite for input %s\n", s);
}

int
main (int argc, char **argv)
{
  test_decimal (0, "0");
  test_decimal (12, "12");
  test_decimal (4711, "4711");
  test_decimal (4294967295, "4294967295");
  test_decimal (4294967290, "4294967290");
#if 0
  /* At the moment, only values up to 2^32 are supported. */
  test_decimal (18446744073709551615ul, "18446744073709551615");
#endif
  test_hex ("", "");
  test_hex ("a", "61");
  test_hex ("\xde\xad\xff", "deadff");

  /* Generated with basenc --base64 */
  test_base64 ("ab", "YWI=");
  test_base64 ("a???b", "YT8/P2I=");
}
