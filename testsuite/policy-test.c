#include "testutils.h"
#include "message.h"
#include "policy.h"
#include <nettle/eddsa.h>

static int
ed25519_verify(const struct public_key *key, const struct signature *sig,
	       size_t size, const uint8_t *data) {
  return ed25519_sha512_verify (key->pub, size, data, sig->s);
}

static void init_policy (struct policy *p)
{
  struct public_key submitter_key = {{60,200,93,138,4,105,67,112,203,245,163,6,229,106,152,53,201,67,12,0,140,168,224,66,200,42,228,13,25,10,245,134}};
  struct public_key log_key = {{21,79,73,151,107,89,255,9,161,35,103,95,88,203,62,52,110,4,85,117,60,60,59,21,212,101,220,180,246,81,43,11}};
  struct public_key witness_key = {{61,241,171,39,199,22,188,88,29,36,213,112,214,128,148,154,139,116,202,249,182,32,148,19,117,223,198,194,131,187,90,78}};

  p->submitter_key = submitter_key;
  p->log_key = log_key;
  p->witness_key = witness_key;
  p->verify = ed25519_verify;
}

int
main(int argc, char **argv)
{
  struct policy policy;
  init_policy (&policy);
  
  // data is "xyz"
  const char *test_msg_hex = "000378797a837a5885e5c5816c654de337e9780929434bad03747ce4a2ce233507430c89f99a8bc03ad8bfe75331e285d57247dbd02b5bfaa613642611fe9f7df81f8aeb0a000000000000050023601a71ef1195e8fa670f33958d3b18cb19812f4ada65f32528afa6acbc88439e223ac58e6121c6c516125f586b3c088e87748412f71b211b2e8788b34a41d3b68b877b0be89009e6df023a19817126885d15aa9ea113d1650a4a2d9b3f04040068366f65eee03b54a5f52f472e3e7c0b7820f0b2fe72481e540fe825ca1486f08c00000000646de1ffc9cf52a492bb52ef09eea95f7b848c5c2081bb53c90bb216382b235bcfbf41005fb4cb3c7adec0e52a3ba10142741b1d072add08285420be1eb01d530652690900000000000004ff0120b01cb54602ff3b3835c4c0a5e690f9dbbb819d6b1062651053d488d171defb0a39caa56614499ce392f53f7bd0e50612650bbddb34e4d14c4b35826d53898db59599341b7734031bf6bd3a9edf3782e1f7951f216b228c2eb4003fb9351bbacc8f3dbb7b6aebe7745d08fcb17214d1bc98dbd4a628e0770b4987551cef8715fa9701e2aa25c978564bf5a59aa1a40064a17f6e741c5a108e5b2e5623d3a96fb2b1e4193fd5e6f1a767716a29873125f268d8d16b919de520b05986e19d4f3b6a39925424d8734aee941b60ef2e00421213288f8d30fd4a12f6060b7e1fe824e2335c10aa94ec8dca25233526069f1ed527ef80d832add6c71c76e6b235027cdaf083a6caeb32773e7b8928d77b26461fabdd70a89b9a6973c50864c50f531fe7";

  uint8_t test_msg[strlen(test_msg_hex) / 2];
  decode_hex(sizeof(test_msg), test_msg, test_msg_hex);

  struct merkle_hash witness_kh;
  policy_get_witness_key_hash (&policy, &witness_kh);

  struct message msg;
  if (!message_parse(&msg, sizeof(test_msg), test_msg, &witness_kh))
    die("failed to parse message\n");
  
  if (policy_verify (&policy, &msg) != 0)
    die("failed to verify message\n");

  return 0;
}
