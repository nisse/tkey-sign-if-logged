#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "testutils.h"

void
die(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);

  abort();
}

static unsigned
hexdigit(char d)
{
  if (d >= '0' && d <= '9')
    return d - '0';
  if (d >= 'a' && d <= 'f')
    return 10 + d - 'a';
  if (d >= 'A' && d <= 'F')
    return 10 + d - 'A';
  die ("Invalid hex digit %c\n", d);
}

void
decode_hex (size_t size, uint8_t *dst, const char *src)
{
  size_t i;
  for (i = 0; i < size; i++)
    {
      unsigned hi = hexdigit(*src++);
      unsigned lo = hexdigit(*src++);
      dst[i] = (hi << 4) | lo;
    }
  if (*src != '\0')
    die ("Invalid length of hex data.\n");
}
