#ifndef TESTUTILS_H_INCLUDED
#define TESTUTILS_H_INCLUDED

#include <stdlib.h>
#include <stdint.h>

void __attribute__((__noreturn__)) __attribute__((__format__ (__printf__, 1,2)))
die(const char *format, ...);

void
decode_hex (size_t size, uint8_t *dst, const char *src);

#endif /* TESTUTILS_H_INCLUDED */
