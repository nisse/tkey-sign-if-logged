# Config

Few notes on how to get started with a working Sigsum setup.

What needs to be hard-coded in the TKey device app is the submitter, log, and
witness public keys.  TODO: witness public key.

Use (dummy) witness key `0x04040000..00` and signature `0x05050000..00`.

## Hints

Create a sigsum submitter key-pair:

    $ go install sigsum.org/sigsum-go/cmd/sigsum-key@latest
    $ sigsum-key gen -o submitter-key

Get log metadata from https://www.sigsum.org/services/:

    $ cat >sigsum.policy <<EOF
    log 154f49976b59ff09a123675f58cb3e346e0455753c3c3b15d465dcb4f6512b0b https://poc.sigsum.org/jellyfish
    quorum none
    EOF

Create keys to hard-code in device app:

    $ cd host/cmd/hex-to-c
    $ sigsum-key hex submitter-key
    $ go run . -hex-key $(sigsum-key hex -k ../../config/submitter-key.pub)
    {60,200,93,138,4,105,67,112,203,245,163,6,229,106,152,53,201,67,12,0,140,168,224,66,200,42,228,13,25,10,245,134}
    $ go run . -hex-key 154f49976b59ff09a123675f58cb3e346e0455753c3c3b15d465dcb4f6512b0b
    {21,79,73,151,107,89,255,9,161,35,103,95,88,203,62,52,110,4,85,117,60,60,59,21,212,101,220,180,246,81,43,11}
    $ go run . -hex-key $(sigsum-key hex -k ../../config/fake-witness-key.pub)
    {61,241,171,39,199,22,188,88,29,36,213,112,214,128,148,154,139,116,202,249,182,32,148,19,117,223,198,194,131,187,90,78}

## Monitor the above public key

Key has we will find matches for:

    $ sigsum-key hash -k submitter-key.pub
    dd5cfe35bfae36a6df8089694c2f1d0c0eda7f154eeb4e0f14faba2ca22222ad

Run from scratch:

    $ go install sigsum.org/sigsum-go/cmd/sigsum-monitor@latest
    $ sigsum-monitor -p sigsum.policy submitter-key.pub
