package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"log"
)

func main() {
	hexKey := flag.String("hex-key", "", "hex-key to output as a C-array")
	flag.Parse()

	var bytesKey [32]byte
	if n, err := hex.Decode(bytesKey[:], []byte(*hexKey)); err != nil {
		log.Fatal(err)
	} else if n != 32 {
		log.Fatal("expected to read 32 bytes")
	}

	output := "{"
	for _, b := range bytesKey[:] {
		output += fmt.Sprintf("%d,", b)
	}
	output = fmt.Sprintf("%s%s", output[:len(output)-1], "}")

	fmt.Printf("%s\n", output)
}
