module local/tkey-signed-if-logged

go 1.19

require (
	github.com/spf13/pflag v1.0.5
	github.com/tillitis/tkeyclient v0.0.0-20230511144543-9ee035fb0288
	go.bug.st/serial v1.5.0
	sigsum.org/sigsum-go v0.3.3
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
