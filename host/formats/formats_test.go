package formats

import (
	"bytes"
	"testing"

	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/proof"
	"sigsum.org/sigsum-go/pkg/types"
)

var (
	testData          = []byte("abcd")
	testLeafIndex     = uint64(1)
	testSize          = uint64(2)
	testTimestamp     = uint64(3)
	testHash          = crypto.Hash{4, 4}
	testSignature     = crypto.Signature{5, 5}
	testShortChecksum = proof.ShortChecksum{6, 6}

	testProof = proof.SigsumProof{
		LogKeyHash: testHash,
		Leaf: proof.ShortLeaf{
			ShortChecksum: testShortChecksum,
			Signature:     testSignature,
			KeyHash:       testHash,
		},
		TreeHead: types.CosignedTreeHead{
			SignedTreeHead: types.SignedTreeHead{
				TreeHead: types.TreeHead{
					Size:     testSize,
					RootHash: testHash,
				},
				Signature: testSignature,
			},
			Cosignatures: []types.Cosignature{
				types.Cosignature{
					KeyHash:   testHash,
					Timestamp: testTimestamp,
					Signature: testSignature,
				},
				types.Cosignature{
					KeyHash:   testHash,
					Timestamp: testTimestamp,
					Signature: testSignature,
				},
			},
		},
		Inclusion: types.InclusionProof{
			LeafIndex: testLeafIndex,
			Path: []crypto.Hash{
				testHash,
			},
		},
	}

	serializedCosignatureLen = byte(len(testHash) + 8 + len(testSignature))
	testSerializedMessage    = bytes.Join([][]byte{
		// data_t
		[]byte{0, byte(len(testData))},
		testData,
		// leaf_t
		testSignature[:],
		// cosigned_tree_head_t
		[]byte{0, 0, 0, 0, 0, 0, 0, byte(testSize)},
		testHash[:],
		testSignature[:],
		[]byte{0, 2 * serializedCosignatureLen},
		bytes.Join([][]byte{ // one cosignature
			testHash[:],
			[]byte{0, 0, 0, 0, 0, 0, 0, byte(testTimestamp)},
			testSignature[:],
		}, nil),
		bytes.Join([][]byte{ // another cosignature
			testHash[:],
			[]byte{0, 0, 0, 0, 0, 0, 0, byte(testTimestamp)},
			testSignature[:],
		}, nil),
		// proof_t
		[]byte{0, 0, 0, 0, 0, 0, 0, byte(testLeafIndex)},
		[]byte{0, 1 * 32},
		testHash[:],
	}, nil)
)

func TestTKeySerialize(t *testing.T) {
	msg := TKeySerialize(testData, testProof)
	if got, want := msg, testSerializedMessage; !bytes.Equal(got, want) {
		t.Errorf("mismatch\ngot:  %x\nwant: %x\n", got, want)
	}
}
