package formats

import (
	"bytes"
	"encoding/binary"

	"sigsum.org/sigsum-go/pkg/proof"
)

func TKeySerialize(data []byte, p proof.SigsumProof) []byte {
	buf := bytes.NewBuffer(nil)

	// data_t
	binary.Write(buf, binary.BigEndian, uint16(len(data)))
	buf.Write(data)

	// leaf_t
	buf.Write(p.Leaf.Signature[:])

	// cosigned_tree_head_t
	binary.Write(buf, binary.BigEndian, p.TreeHead.Size)
	buf.Write(p.TreeHead.RootHash[:])
	buf.Write(p.TreeHead.Signature[:])
	binary.Write(buf, binary.BigEndian, uint16(104*len(p.TreeHead.Cosignatures)))
	for _, cosignature := range p.TreeHead.Cosignatures {
		buf.Write(cosignature.KeyHash[:])
		binary.Write(buf, binary.BigEndian, cosignature.Timestamp)
		buf.Write(cosignature.Signature[:])
	}

	// proof_t
	binary.Write(buf, binary.BigEndian, p.Inclusion.LeafIndex)
	binary.Write(buf, binary.BigEndian, uint16(32*len(p.Inclusion.Path)))
	for _, hash := range p.Inclusion.Path {
		buf.Write(hash[:])
	}

	return buf.Bytes()
}
