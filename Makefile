all:
	for d in app host testsuite ; do \
		(cd $$d && $(MAKE) all) ; \
	done

check:
	cd testsuite && $(MAKE) check

.PHONY: all check
